import { Router } from 'express';
import rateLimit from 'express-rate-limit';
import { login, register, logOut, isAuthenticated } from '../controllers/auth';

const router = Router();

const limiter = rateLimit({
  max: 10,
  windowMs: 5 * 60 * 1000,
  message: 'Too many requests within 5 minutes. Please try again later.'
});

router.post('/register', register);
router.post('/login', limiter, login);
router.post('/logout', logOut);
router.get('/isAuthenticated', isAuthenticated);

export default router;
