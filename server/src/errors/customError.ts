class CustomApiError extends Error {
  statusCode: number;

  constructor(message: string, statusCode: number) {
    super(message);
    this.statusCode = statusCode;
  }
}

const createCustomError = (message: string, statusCode: number) => {
  return new CustomApiError(message, statusCode);
};

export { CustomApiError, createCustomError };
