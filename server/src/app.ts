import 'dotenv/config';

import mongoose from 'mongoose';
import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import rateLimit from 'express-rate-limit';
import helmet from 'helmet';
import mongoSanitize from 'express-mongo-sanitize';
import hpp from 'hpp';

import invoiceRouter from './routes/invoice';
import authRouter from './routes/auth';

import notFound from './middleware/notFound';
import errorHandlerMiddleware from './middleware/errorHandler';

const app = express();

app.use(
  cors({
    origin: 'https://invoice-app-inithar.netlify.app',
    credentials: true
  })
);

const limiter = rateLimit({
  max: 100,
  windowMs: 5 * 60 * 1000,
  message: 'Too many requests within 5 minutes. Please try again later.'
});

app.use(limiter);
app.use(helmet());
app.use(mongoSanitize());
app.use(hpp());

app.use(express.json());
app.use(cookieParser());

app.use('/api/v1/invoices', invoiceRouter);
app.use('/api/v1/auth', authRouter);
app.use(notFound);

app.use(errorHandlerMiddleware);

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI as string);
    app.listen(process.env.PORT || 3000);
  } catch (error) {
    console.log(error);
  }
};

start();
