import { Secret, verify } from 'jsonwebtoken';

const verifyToken = (token: string, secret: Secret) => {
  return new Promise((resolve, reject) => {
    verify(token, secret, (err, decoded) => {
      err ? reject(err) : resolve(decoded);
    });
  });
};

export default verifyToken;
