import { JwtPayload } from 'jsonwebtoken';

import verifyToken from './verifyToken';
import User from '../models/user';

const findUserByJwt = async (token: string) => {
  const decoded = (await verifyToken(token, process.env.JWT_KEY!)) as JwtPayload;
  return await User.findById(decoded.id);
};

export default findUserByJwt;
