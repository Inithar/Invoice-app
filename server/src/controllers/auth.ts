import { Request, Response, NextFunction } from 'express';
import { JwtPayload, sign } from 'jsonwebtoken';
import bcrypt from 'bcrypt';

import User, { UserDocument } from '../models/user';
import { createCustomError } from '../errors/customError';
import catchAsync from '../middleware/catchAsync';
import verifyToken from '../utils/verifyToken';

const createAndSendToken = (res: Response, user: UserDocument, statusCode: number) => {
  const token = sign({ id: user._id }, process.env.JWT_KEY!, {
    expiresIn: process.env.JWT_EXPIRES_IN
  });

  const expiresIn = Number(process.env.JWT_COOKIE_EXPIRES_IN) * 24 * 60 * 60 * 1000;

  res.cookie('jwt', token, {
    expires: new Date(Date.now() + expiresIn),
    httpOnly: true,
    secure: true
  });

  const { password, ...userWithoutPassword } = user._doc;

  res.status(statusCode).json({
    user: userWithoutPassword,
    token
  });
};

const register = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const { email, password, passwordConfirm } = req.body;
  const user = await User.findOne({ email });

  if (user) {
    return next(createCustomError('Email is already in use', 400));
  }

  if (password !== passwordConfirm) {
    return next(createCustomError('Passwords are not the same', 400));
  }

  const hashedPassword = await bcrypt.hash(password, 12);
  const newUser = await User.create({ email, password: hashedPassword });
  createAndSendToken(res, newUser, 201);
});

const login = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(createCustomError('Missing email or password', 400));
  }

  const user = await User.findOne({ email }).select('+password');

  if (!user || !(await user.isPasswordCorrect(password, user.password))) {
    return next(createCustomError('Invalid email or password', 401));
  }

  createAndSendToken(res, user, 200);
});

const logOut = (req: Request, res: Response, next: NextFunction) => {
  res.clearCookie('jwt');
  res.status(200).json({ status: 'success' });
};

const isAuthenticated = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const token = req.cookies.jwt;
  let user = null;

  if (token) {
    const decoded = (await verifyToken(token, process.env.JWT_KEY!)) as JwtPayload;
    user = await User.findById(decoded.id);
  }

  res.status(200).json({ user, token });
});

export { register, login, logOut, isAuthenticated };
