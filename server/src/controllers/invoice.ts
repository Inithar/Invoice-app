import { Request, Response, NextFunction } from 'express';

import { createCustomError } from '../errors/customError';
import catchAsync from '../middleware/catchAsync';
import Invoice from '../models/invoice';
import findUserByJwt from '../utils/findUserByJwt';

const getAllInvoices = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const user = await findUserByJwt(req.cookies.jwt);
  const invoices = await Invoice.find({ userId: user!._id });
  res.status(200).json(invoices);
});

const getInvoice = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const invoice = await Invoice.findOne({ shortId: id });

  if (!invoice) {
    return next(createCustomError(`No invoice with id : ${id}`, 404));
  }

  res.status(200).json(invoice);
});

const createInvoice = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const user = await findUserByJwt(req.cookies.jwt);
  const invoice = new Invoice({ userId: user!._id, ...req.body.invoice });
  const createdInvoice = req.body.asDraft ? await invoice.save({ validateBeforeSave: false }) : await invoice.save();
  res.status(201).json(createdInvoice);
});

const updateInvoice = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const invoice = await Invoice.findOneAndUpdate({ shortId: id }, req.body, {
    new: true,
    runValidators: true
  });

  if (!invoice) {
    return next(createCustomError(`No invoice with id : ${id}`, 404));
  }

  res.status(200).json(invoice);
});

const deleteInvoice = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const invoice = await Invoice.findOneAndDelete({ shortId: id });

  if (!invoice) {
    return next(createCustomError(`No invoice with id : ${id}`, 404));
  }

  res.status(204).json();
});

export { getAllInvoices, getInvoice, createInvoice, updateInvoice, deleteInvoice };
