import { Schema, model } from 'mongoose';
import { nanoid } from 'nanoid';

const InvoiceSchema = new Schema({
  shortId: String,
  userId: {
    type: Schema.Types.ObjectId,
    required: [true, 'User ID is required']
  },
  clientName: {
    type: String,
    required: [true, "Client's can't be empty"]
  },
  clientEmail: {
    type: String,
    required: [true, "Email can't be empty"]
  },
  status: {
    type: String,
    enum: ['paid', 'pending', 'draft']
  },
  description: {
    type: String,
    required: [true, "Description can't be empty"]
  },
  invoiceDate: Date,
  paymentDue: Date,
  senderAddress: {
    street: {
      type: String,
      required: [true, "Street Address can't be empty"]
    },
    city: {
      type: String,
      required: [true, "City can't be empty"]
    },
    postCode: {
      type: String,
      required: [true, "Post Code can't be empty"]
    },
    country: {
      type: String,
      required: [true, "Country can't be empty"]
    }
  },
  clientAddress: {
    street: {
      type: String,
      required: [true, "Street Address can't be empty"]
    },
    city: {
      type: String,
      required: [true, "City can't be empty"]
    },
    postCode: {
      type: String,
      required: [true, "Post Code can't be empty"]
    },
    country: {
      type: String,
      required: [true, "Country can't be empty"]
    }
  },
  items: [
    {
      name: {
        type: String,
        required: [true, "Name can't be empty"]
      },
      quantity: Number,
      price: {
        type: Number,
        required: [true, "Price can't be empty"]
      }
    }
  ]
});

InvoiceSchema.pre('save', function (next) {
  this.shortId = nanoid(6);
  next();
});

type Address = {
  street: string;
  city: string;
  postCode: string;
  country: string;
};

type Item = {
  name: string;
  quantity: number;
  price: number;
};

export interface InvoiceDocument extends Document {
  shortId: string;
  clientName: string;
  status: string;
  description: string;
  invoiceDate: Date;
  paymentDue: Date;
  senderAddress: Address;
  clientAddress: Address;
  items: Item[];
}

export default model<InvoiceDocument>('Invoice', InvoiceSchema);
