import { Document, Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';

const UserSchema = new Schema({
  email: {
    type: String,
    required: [true, "Email can't be empty"],
    unique: true,
    lowercase: true
  },
  password: {
    type: String,
    required: [true, "Password can't be empty"],
    minlength: [8, 'Password must be at least 8 characters long'],
    select: false
  }
});

UserSchema.methods.isPasswordCorrect = async function (passwordToVerify: string, userPassword: string) {
  return await bcrypt.compare(passwordToVerify, userPassword);
};

export interface User {
  email: string;
  password: string;
}

export interface UserDocument extends User, Document {
  _doc: any;
  isPasswordCorrect(passwordToVerify: string, userPassword: string): Promise<boolean>;
}

export default model<UserDocument>('User', UserSchema);
