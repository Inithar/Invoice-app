import { Request, Response, NextFunction } from 'express';

type Callback = (req: Request, res: Response, next: NextFunction) => Promise<void>;

const catchAsync = (callback: Callback) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await callback(req, res, next);
    } catch (error) {
      next(error);
    }
  };
};

export default catchAsync;
