import { Request, Response, NextFunction } from 'express';

import { CustomApiError } from '../errors/customError';

const errorHandlerMiddleware = (err: Error, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof CustomApiError) {
    return res.status(err.statusCode).json({ message: err.message });
  }

  return res.status(500).json({ message: 'Something went wrong, try again later.' });
};

export default errorHandlerMiddleware;
