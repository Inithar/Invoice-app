# Invoice App

Full-stack invoice management application.
Design imported from [frontendmentor/room-homepage](https://www.frontendmentor.io/challenges/invoice-app-i7KaLTQjl).

## Table of contents

- [Status](#status)
- [Features](#user-can)
- [Build with](#build-with)
- [Screenshots](#screenshots)
- [To do](#to-do)
- [Contact](#contact)

## Status

#### In progress

#### Live version: soon

## User can:

- register / login
- create, read, update, and delete invoices
- save draft invoices, and mark pending invoices as paid
- filter invoices by status (draft/pending/paid)
- toggle light and dark mode

## Built with:

- React
- React Hooks
- Redux Toolkit
- RTK Query
- TypeScript
- SCSS
- Express
- MongoDB

## Screenshots

<p align="center">
  <img src="https://user-images.githubusercontent.com/72702964/234794651-a839b03f-f0c1-4421-b1b9-a160e4183426.png" alt="home page">
</p>

<p align="center">
  <img src="https://user-images.githubusercontent.com/72702964/234794648-928effb9-725b-443e-8d59-6332c4685af0.png" alt="home page">
</p>

<p align="center">
  <img src="https://user-images.githubusercontent.com/72702964/234794641-f1634a04-0182-44c4-b282-ca2486dd16de.png" alt="home page">
</p>

## To do:

- [x] implement register / login
- [x] implement creating, reading, updating, and deleting invoices
- [x] implement saving draft invoices, and mark pending invoices as paid
- [x] implement filtering invoices by status (draft/pending/paid)
- [x] implement form validation when the user is trying to create/edit an invoice.
- [x] add toggle light and dark mode

## Contact

#### Contact me via e-mail: szymon.switala@proton.me
