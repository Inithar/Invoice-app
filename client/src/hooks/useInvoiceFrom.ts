import { ChangeEvent } from 'react';
import { useAppDispatch } from './useAppDispatch';
import { useAppSelector } from './useAppSelector';
import { RootState } from '../redux/store';
import {
  setSenderAddressProperty,
  setClientAddressProperty,
  setInvoiceDetails,
  setItem,
  addItem,
  deleteItem,
  setInitialState,
} from '../redux/slices/invoiceSlice';

const useInvoiceFrom = () => {
  const dispatch = useAppDispatch();
  const invoice = useAppSelector((state: RootState) => state.invoice);

  const handleSenderAddressChange = (e: ChangeEvent<HTMLInputElement>) => {
    const changedProperty = { key: e.target.name, value: e.target.value };
    dispatch(setSenderAddressProperty(changedProperty));
  };

  const handleClientAddressChange = (e: ChangeEvent<HTMLInputElement>) => {
    const name = e.target.name;
    const key = name.split('client')[1].charAt(0).toLowerCase() + name.split('client')[1].slice(1);
    const changedProperty = { key, value: e.target.value };
    dispatch(setClientAddressProperty(changedProperty));
  };

  const handleInvoiceDetailsChange = (e: ChangeEvent<HTMLInputElement>) => {
    const changedProperty = { key: e.target.name, value: e.target.value };
    dispatch(setInvoiceDetails(changedProperty));
  };

  const handleItemChange = (e: ChangeEvent<HTMLInputElement>, id: string) => {
    const changedItemProperty = { key: e.target.name, value: e.target.value, id };
    dispatch(setItem(changedItemProperty));
  };

  const handleNewItem = () => {
    dispatch(addItem());
  };

  const handleDeleteItem = (id: string) => {
    dispatch(deleteItem(id));
  };

  const clearFrom = () => {
    dispatch(setInitialState());
  };

  const checkRequiredInputs = () => {
    const errors = [];
    const { senderAddress, clientAddress, items, ...invoiceDetails } = invoice;

    if (Object.values(senderAddress).includes('') || Object.values(clientAddress).includes('') || Object.values(invoiceDetails).includes('')) {
      errors.push('All fields must be added');
    }

    if (items.length === 0) {
      errors.push('An item must be added');
    }

    if (items.length > 0) {
      items.forEach((item) => {
        if (Object.values(item).includes('')) {
          errors.push('All item fields must be added');
          return;
        }
      });
    }

    return errors;
  };

  return {
    handleSenderAddressChange,
    handleClientAddressChange,
    handleInvoiceDetailsChange,
    handleItemChange,
    handleNewItem,
    handleDeleteItem,
    clearFrom,
    checkRequiredInputs,
  };
};

export default useInvoiceFrom;
