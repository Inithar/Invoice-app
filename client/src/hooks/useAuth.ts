import { setCredentials, setIsLoading } from '../redux/slices/authSlice';
import { useLazyIsAuthenticatedQuery } from '../redux/api/authApi';
import { useAppDispatch } from './useAppDispatch';

const useAuth = () => {
  const dispatch = useAppDispatch();
  const [isAuthenticatedQuery] = useLazyIsAuthenticatedQuery();

  const isAuthenticated = async () => {
    const response = await isAuthenticatedQuery();
    dispatch(setIsLoading(false));

    if (response.data?.user) {
      const { user, token } = response.data;
      dispatch(setCredentials({ user, token }));
    }
  };

  return { isAuthenticated };
};

export default useAuth;
