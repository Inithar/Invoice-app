import styles from './InvoiceModal.module.scss';

import { createPortal } from 'react-dom';

import { useState } from 'react';
import { useAppSelector } from '../../../hooks/useAppSelector';
import { RootState } from '../../../redux/store';
import useWindowWidth from '../../../hooks/useWindowWidth';
import useInvoiceFrom from '../../../hooks/useInvoiceFrom';

import InputField from '../../atoms/InputField';
import Text from '../../atoms/Text';
import ItemList from '../../molecules/invoiceModal/ItemList';
import Controls from '../../molecules/invoiceModal/Controls';
import BackButton from '../../molecules/invoiceModal/BackButton';
import InvoiceTerms from '../../molecules/invoiceModal/InvoiceTerms';

interface InvoiceModalProps {
  open: boolean;
  type: 'new' | 'edit';
  onClose: () => void;
}

const InvoiceModal = ({ open, onClose, type }: InvoiceModalProps) => {
  const isMobile = useWindowWidth() < 576;
  const [errors, setErrors] = useState<string[]>([]);

  const { senderAddress, clientAddress, items, ...invoiceDetails } = useAppSelector((state: RootState) => state.invoice);
  const { handleSenderAddressChange, handleClientAddressChange, handleInvoiceDetailsChange } = useInvoiceFrom();

  if (!open) {
    return null;
  }

  return createPortal(
    <>
      <div className={styles.overlay}></div>

      <div className={styles.modal}>
        <div className={styles.modal_content}>
          {isMobile && <BackButton onClick={onClose} />}

          <h2 className={styles.heading}>New Invoice</h2>

          <form>
            <Text className={styles.section_name}>Bill From</Text>

            <div className={styles.bill_from_box}>
              <InputField
                name="street"
                label="Street Address"
                className={styles.street}
                value={senderAddress.street}
                onChange={handleSenderAddressChange}
                required
              />
              <InputField name="city" label="City" value={senderAddress.city} onChange={handleSenderAddressChange} required />
              <InputField name="postCode" label="Post Code" value={senderAddress.postCode} onChange={handleSenderAddressChange} required />
              <InputField
                name="country"
                label="Country"
                className={styles.country}
                value={senderAddress.country}
                onChange={handleSenderAddressChange}
                required
              />
            </div>

            <Text className={styles.section_name}>Bill To</Text>

            <div className={styles.bill_to_box}>
              <InputField name="clientName" label="Client's Name" value={invoiceDetails.clientName} onChange={handleInvoiceDetailsChange} required />
              <InputField
                name="clientEmail"
                label="Client's Email"
                type="email"
                value={invoiceDetails.clientEmail}
                onChange={handleInvoiceDetailsChange}
                errorMsg="Invalid email address"
                required
              />

              <div className={styles.bill_to_box_address}>
                <InputField
                  name="clientStreet"
                  label="Street Address"
                  className={styles.street}
                  value={clientAddress.street}
                  onChange={handleClientAddressChange}
                  required
                />
                <InputField name="clientCity" label="City" value={clientAddress.city} onChange={handleClientAddressChange} required />
                <InputField name="clientPostCode" label="Post Code" value={clientAddress.postCode} onChange={handleClientAddressChange} required />
                <InputField
                  name="clientCountry"
                  label="Country"
                  className={styles.country}
                  value={clientAddress.country}
                  onChange={handleClientAddressChange}
                  required
                />
              </div>

              <InvoiceTerms />

              <InputField
                name="description"
                label="Project Description"
                value={invoiceDetails.description}
                onChange={handleInvoiceDetailsChange}
                required
              />
            </div>

            <ItemList items={items} />

            <div className={styles.errors} data-error={errors.length > 0}>
              {errors?.map((error) => (
                <p className={styles.error} key={crypto.randomUUID()}>
                  - {error}
                </p>
              ))}
            </div>

            <Controls type={type} setErrors={setErrors} closeModal={onClose} />
          </form>
        </div>
      </div>
    </>,
    document.getElementById('portal') as HTMLElement
  );
};

export default InvoiceModal;
