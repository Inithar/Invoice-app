import styles from './Navigation.module.scss';

import { FiLogOut } from 'react-icons/fi';
import { useNavigate } from 'react-router-dom';

import { useLogOutMutation } from '../../../redux/api/authApi';
import { useAppDispatch } from '../../../hooks/useAppDispatch';
import { logOut } from '../../../redux/slices/authSlice';

import ThemeToggle from '../../atoms/ThemeToggle';

const Navigation = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [logOutMutation] = useLogOutMutation();

  const handleLogOut = async () => {
    await logOutMutation();
    dispatch(logOut());
    window.location.reload();
    navigate('/login');
  };

  return (
    <nav className={styles.nav}>
      <div className={styles.left}>
        <img src="/assets/logo.svg" alt="logo" className={styles.logo} />
        <ThemeToggle />
      </div>
      <div className={styles.right}>
        <FiLogOut className={styles.logout_icon} onClick={handleLogOut} />
      </div>
    </nav>
  );
};

export default Navigation;
