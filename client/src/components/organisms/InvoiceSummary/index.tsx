import styles from './InvoiceSummary.module.scss';

import { Item } from '../../../types/invoiceApi';
import countTotalAmount from '../../../utils/countTotalAmount';

import Text from '../../atoms/Text';

interface InvoiceSummaryProps {
  items: Item[];
}

const InvoiceSummary = ({ items }: InvoiceSummaryProps) => (
  <div className={styles.summary}>
    <div className={styles.item_list}>
      <div className={styles.head}>
        <Text variant="secondary">Item Name</Text>
        <Text variant="secondary" className={styles.qty}>
          QTY.
        </Text>
        <Text variant="secondary" className={styles.price}>
          Price
        </Text>
        <Text variant="secondary" className={styles.total}>
          Total
        </Text>
      </div>

      <div>
        {items.map(({ name, quantity, price }) => (
          <div className={styles.item} key={crypto.randomUUID()}>
            <Text>{name}</Text>
            <Text className={styles.qty}>{quantity}</Text>
            <Text className={styles.price}>£ {price}</Text>
            <Text className={styles.total}>£ {quantity * price}</Text>
          </div>
        ))}
      </div>
    </div>

    <div className={styles.amount_due}>
      <Text variant="secondary">Amount Due</Text>
      <p className={styles.total_amount}>£ {countTotalAmount(items)}</p>
    </div>
  </div>
);

export default InvoiceSummary;
