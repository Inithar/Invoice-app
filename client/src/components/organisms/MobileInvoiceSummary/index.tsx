import styles from './MobileInvoiceSummary.module.scss';

import { Item } from '../../../types/invoiceApi';
import countTotalAmount from '../../../utils/countTotalAmount';

import Text from '../../atoms/Text';

interface MobileInvoiceSummary {
  items: Item[];
}

const MobileInvoiceSummary = ({ items }: MobileInvoiceSummary) => (
  <div className={styles.summary}>
    <div className={styles.item_list}>
      {items.map(({ name, quantity, price }) => (
        <div className={styles.item} key={crypto.randomUUID()}>
          <div>
            <Text>{name}</Text>
            <Text variant="secondary">
              {quantity} x £ {price}
            </Text>
          </div>
          <Text>£ {quantity * price}</Text>
        </div>
      ))}
    </div>

    <div className={styles.amount_due}>
      <Text variant="secondary">Amount Due</Text>
      <p className={styles.total_amount}>£ {countTotalAmount(items)}</p>
    </div>
  </div>
);

export default MobileInvoiceSummary;
