import styles from './Modal.module.scss';

import { createPortal } from 'react-dom';
import classNames from 'classnames/bind';

interface ModalProps {
  children: JSX.Element | JSX.Element[];
  open: boolean;
  className?: string;
}

const Modal = ({ children, open, className }: ModalProps) => {
  const cx = classNames.bind(styles);
  const modalClassName = cx(className, 'modal');

  if (!open) {
    return null;
  }

  return createPortal(
    <>
      <div className={styles.overlay}></div>
      <div className={modalClassName}>{children}</div>;
    </>,
    document.getElementById('portal') as HTMLElement
  );
};

export default Modal;
