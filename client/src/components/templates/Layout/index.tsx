import styles from './Layout.module.scss';

import { Outlet } from 'react-router-dom';
import Navigation from '../../organisms/Navigation';

const Layout = () => (
  <div className={styles.wrapper}>
    <Navigation />
    <div className={styles.container}>
      <Outlet />
    </div>
  </div>
);

export default Layout;
