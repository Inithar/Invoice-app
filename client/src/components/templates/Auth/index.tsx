import styles from './Auth.module.scss';
import { Outlet, useNavigate } from 'react-router-dom';

import { useAppSelector } from '../../../hooks/useAppSelector';
import { useEffect } from 'react';

const Auth = () => {
  const token = useAppSelector((state) => state.auth.token);
  const navigate = useNavigate();

  useEffect(() => {
    if (token) navigate('/');
  }, [token]);

  return (
    <main className={styles.wrapper}>
      <div className={styles.auth_box}>
        <Outlet />
      </div>
    </main>
  );
};

export default Auth;
