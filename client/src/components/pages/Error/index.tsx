import styles from './Error.module.scss';

import { Link } from 'react-router-dom';

import Text from '../../atoms/Text';
import Button from '../../atoms/Button';

interface ErrorProps {
  type: 'notFound' | 'error';
}

const Error = ({ type }: ErrorProps) => {
  const isError = type === 'error';

  return (
    <main className={styles.container}>
      <img src={`/assets/${isError ? 'error' : 'page-not-found'}.svg`} alt="404 error image" />
      <div className={styles.text_container}>
        <h1>{isError ? 'Something went wrong...' : 'Page not found...'}</h1>
        <Text variant="secondary">
          {isError ? 'We are working on fixing the problem. Try again later' : 'The page is missing or you assembled the link incorrectly'}
        </Text>
        <Link to="/">
          <Button>Go to homepage</Button>
        </Link>
      </div>
    </main>
  );
};

export default Error;
