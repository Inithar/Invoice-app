import styles from './Home.module.scss';

import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import useWindowWidth from '../../../hooks/useWindowWidth';
import { useGetAllInvoicesQuery } from '../../../redux/api/invoicesApi';
import { InvoiceStatus } from '../../../types/invoiceApi';

import Button from '../../atoms/Button';
import Wrapper from '../../atoms/Wrapper';
import Text from '../../atoms/Text';
import Loader from '../../atoms/Loader';
import InvoiceBox from '../../molecules/InvoiceBox';
import MultiSelectMenu from '../../molecules/MultiSelectMenu';
import InvoiceModal from '../../organisms/InvoiceModal';

const Home = () => {
  const navigate = useNavigate();
  const isMobile = useWindowWidth() < 576;

  const [isModalOpen, setIsModalOpen] = useState(false);

  const { data, isLoading, isError } = useGetAllInvoicesQuery();
  const [filterBy, setFilterBy] = useState<Record<InvoiceStatus, boolean>>({ draft: false, paid: false, pending: false });
  const [filteredInvoices, setFilteredInvoices] = useState(data);

  useEffect(() => {
    if (isError) navigate('/error');
    if (data) setFilteredInvoices([...data].reverse());
  }, [isError, data]);

  useEffect(() => {
    if (data) {
      const filterByArr = Object.keys(filterBy).filter((key) => filterBy[key as InvoiceStatus] === true);
      setFilteredInvoices(filterByArr.length ? data.filter((invoice) => filterByArr.includes(invoice.status)) : [...data].reverse());
    }
  }, [filterBy]);

  const countInvoices = () => {
    const numberOfInvoices = data?.length;
    let message = 'No invoices';

    if (numberOfInvoices) {
      message = isMobile ? `${numberOfInvoices} invoices` : `There are ${numberOfInvoices} total invoices`;
    }

    return message;
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <div className={styles.container}>
      <Wrapper>
        <header className={styles.header}>
          <div className={styles.header_left_section}>
            <h1>Invoices</h1>
            <Text variant="secondary">{countInvoices()}</Text>
          </div>

          <div className={styles.header_right_section}>
            <MultiSelectMenu
              options={filterBy}
              placeholder={isMobile ? 'Filter' : 'Filter by status'}
              onChange={(option, isChecked) => {
                setFilterBy({ ...filterBy, [option]: isChecked });
              }}
            />
            <Button icon variant="primary" onClick={() => setIsModalOpen(true)}>
              {isMobile ? 'New' : 'New Invoice'}
            </Button>
          </div>
        </header>

        <main className={styles.invoices}>
          {data?.length ? (
            filteredInvoices?.map(({ shortId, status, clientName, items, paymentDue }) => (
              <Link to={`invoices/${shortId}`} className={styles.link} key={crypto.randomUUID()}>
                <InvoiceBox id={shortId} clientName={clientName} status={status} items={items} paymentDue={paymentDue} />
              </Link>
            ))
          ) : (
            <div className={styles.no_invoices}>
              <img src="/assets/no-invoices.png" alt="A woman holding a megaphone in an open envelope" />
              <h2 className={styles.no_invoices_heading}>There is nothing here</h2>
              <Text variant="secondary" className={styles.no_invoices_text}>
                Create a new invoice by clicking the <span onClick={() => setIsModalOpen(true)}>New Invoice</span> button and get started
              </Text>
            </div>
          )}
        </main>

        <InvoiceModal type="new" open={isModalOpen} onClose={() => setIsModalOpen(false)} />
      </Wrapper>
    </div>
  );
};

export default Home;
