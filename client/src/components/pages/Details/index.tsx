import styles from './Details.module.scss';

import { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';

import formatDate from '../../../utils/formatDate';
import useWindowWidth from '../../../hooks/useWindowWidth';
import { useAppDispatch } from '../../../hooks/useAppDispatch';
import { setInvoice } from '../../../redux/slices/invoiceSlice';
import { useGetInvoiceQuery, useDeleteInvoiceMutation, useEditInvoiceMutation } from '../../../redux/api/invoicesApi';

import Status from '../../atoms/Status';
import Button from '../../atoms/Button';
import Text from '../../atoms/Text';
import Wrapper from '../../atoms/Wrapper';
import Loader from '../../atoms/Loader';
import MobileInvoiceSummary from '../../organisms/MobileInvoiceSummary';
import InvoiceSummary from '../../organisms/InvoiceSummary';
import Modal from '../../organisms/Modal';
import InvoiceModal from '../../organisms/InvoiceModal';

const Details = () => {
  const isMobile = useWindowWidth() < 576;
  const { id } = useParams();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isInvoiceModalOpen, setIsInvoiceModalOpen] = useState(false);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [deleteInvoice] = useDeleteInvoiceMutation();
  const [editInvoice, editInvoiceDetails] = useEditInvoiceMutation();
  const { data, isLoading, isError: isGetInvoiceError } = useGetInvoiceQuery(id);

  useEffect(() => {
    if (editInvoiceDetails.isError || isGetInvoiceError) {
      navigate('/error');
    }
  }, [editInvoiceDetails, isGetInvoiceError]);

  const handleInvoiceDelete = async (id: string) => {
    setIsModalOpen(false);
    const deleteInvoiceDetails = await deleteInvoice(id);
    navigate(`${'error' in deleteInvoiceDetails ? '/error' : '/'}`);
  };

  const handleEditButton = () => {
    const { _id, shortId, paymentDue, ...dataToSet } = data!;
    const numberOfDaysInMilliseconds = new Date(paymentDue).getTime() - new Date(invoiceDate).getTime();
    const daysToPayment = Math.ceil(numberOfDaysInMilliseconds / (1000 * 3600 * 24));
    const invoice = { daysToPayment, ...dataToSet };
    dispatch(setInvoice(invoice));
    setIsInvoiceModalOpen(true);
  };

  const handleMarkAsPaid = () => {
    const { _id, shortId, ...dataToSet } = data!;
    dataToSet.status = 'paid';
    editInvoice({ invoice: dataToSet, id: shortId });
  };

  if (isLoading) {
    return <Loader />;
  }

  const { shortId, status, senderAddress, clientAddress, clientEmail, clientName, items, paymentDue, invoiceDate } = data!;

  return (
    <main className={styles.main}>
      <Wrapper>
        <Link to="/" className={styles.link}>
          <img src="/assets/arrow-left.svg" alt="left arrow" />
          Go back
        </Link>

        <div className={styles.top_section}>
          <div className={styles.status}>
            <Text variant="secondary">Status</Text>
            <Status status={status} />
          </div>
          <div className={styles.controls}>
            <Button variant="secondary" onClick={handleEditButton}>
              Edit
            </Button>
            <Button variant="danger" onClick={() => setIsModalOpen(true)}>
              Delete
            </Button>
            {status === 'pending' && (
              <Button variant="primary" onClick={handleMarkAsPaid}>
                Mark as Paid
              </Button>
            )}
          </div>
        </div>

        <div className={styles.details}>
          <div className={styles.sender_details}>
            <div>
              <p className={styles.id}>
                <span>#</span>
                {shortId}
              </p>
              <Text variant="secondary">Graphic Design</Text>
            </div>
            <div className={styles.address}>
              <Text variant="secondary">{senderAddress.street}</Text>
              <Text variant="secondary">{senderAddress.city}</Text>
              <Text variant="secondary">{senderAddress.postCode}</Text>
              <Text variant="secondary">{senderAddress.country}</Text>
            </div>
          </div>

          <div className={styles.recipient_details}>
            <div>
              <Text variant="secondary">Invoice Date</Text>
              <Text>{invoiceDate ? formatDate(new Date(invoiceDate)) : ''}</Text>
            </div>
            <div className={styles.identities}>
              <Text variant="secondary">Bill To</Text>
              <Text>{clientName}</Text>
              <Text variant="secondary" className={styles.street}>
                {clientAddress.street}
              </Text>
              <Text variant="secondary"> {clientAddress.city}</Text>
              <Text variant="secondary">{clientAddress.postCode}</Text>
              <Text variant="secondary">{clientAddress.country}</Text>
            </div>
            <div>
              <Text variant="secondary">Payment Due</Text>
              <Text>{paymentDue ? formatDate(new Date(paymentDue)) : ''}</Text>
            </div>
            <div className={styles.send_to}>
              <Text variant="secondary">Sent to</Text>
              <Text>{clientEmail}</Text>
            </div>
          </div>

          {isMobile ? <MobileInvoiceSummary items={items} /> : <InvoiceSummary items={items} />}
        </div>
      </Wrapper>

      <Modal open={isModalOpen} className={styles.modal}>
        <h2 className={styles.modal_heading}>Confirm Deletion</h2>
        <Text variant="secondary" className={styles.modal_text}>
          Are you sure you want to delete invoice #{shortId}? This action cannot be undone.
        </Text>
        <div className={styles.modal_controls}>
          <Button variant="secondary" onClick={() => setIsModalOpen(false)}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => handleInvoiceDelete(shortId)} className={styles.test}>
            Delete
          </Button>
        </div>
      </Modal>

      <InvoiceModal type="edit" open={isInvoiceModalOpen} onClose={() => setIsInvoiceModalOpen(false)} />
    </main>
  );
};

export default Details;
