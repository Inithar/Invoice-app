import styles from './Login.module.scss';

import { ChangeEvent, FormEvent, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { useAppDispatch } from '../../../hooks/useAppDispatch';
import { useLoginMutation } from '../../../redux/api/authApi';
import { setCredentials, setIsLoading } from '../../../redux/slices/authSlice';
import { ApiError } from '../../../types/api';

import Button from '../../atoms/Button';
import Text from '../../atoms/Text';
import UnderlineInput from '../../atoms/UnderlineInput';

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [loginData, setLoginData] = useState({ email: '', password: '' });
  const [error, setError] = useState('');
  const [login] = useLoginMutation();

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setLoginData({ ...loginData, [e.target.name]: e.target.value });
  };

  const handleLogin = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (!loginData.email || !loginData.password) {
      setError("Email address and password can't be empty");
      return;
    }

    const userData = await login(loginData);
    dispatch(setIsLoading(false));

    if ('data' in userData) {
      const { user, token } = userData.data;
      dispatch(setCredentials({ user, token }));
      navigate('/');
    }

    if ('error' in userData) {
      setError(
        'originalStatus' in userData.error && userData.error.originalStatus === 429 ? userData.error.data : (userData.error as ApiError).data.message
      );
    }
  };

  return (
    <>
      <h1 className={styles.heading}>Login</h1>

      <form className={styles.login_form} onSubmit={handleLogin} noValidate>
        <UnderlineInput
          name="email"
          placeholder="Email address"
          type="email"
          value={loginData.email}
          required
          errorMsg="Invalid email address"
          onChange={handleInputChange}
        />
        <UnderlineInput name="password" placeholder="Password" type="password" value={loginData.password} required onChange={handleInputChange} />
        <Button>Login to your account</Button>
      </form>

      <p className={styles.error}>{error}</p>

      <Text className={styles.text}>
        Don't have an account? <Link to="/register">Sign Up</Link>
      </Text>
    </>
  );
};

export default Login;
