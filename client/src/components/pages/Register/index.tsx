import styles from './Register.module.scss';

import { ChangeEvent, useState, FormEvent } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { useRegisterMutation } from '../../../redux/api/authApi';
import { useAppDispatch } from '../../../hooks/useAppDispatch';
import { setCredentials, setIsLoading } from '../../../redux/slices/authSlice';
import { ApiError } from '../../../types/api';

import Button from '../../atoms/Button';
import Text from '../../atoms/Text';
import UnderlineInput from '../../atoms/UnderlineInput';

const Register = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [register] = useRegisterMutation();
  const [registerData, setRegisterData] = useState({ email: '', password: '', passwordConfirm: '' });
  const [error, setError] = useState('');

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    setRegisterData({ ...registerData, [e.target.name]: e.target.value });
  };

  const handleRegister = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (!registerData.email || !registerData.password) {
      setError("Email address and password can't be empty");
      return;
    }

    if (registerData.password !== registerData.passwordConfirm) {
      setError('Passwords are not the same');
      return;
    }

    const userData = await register(registerData);

    dispatch(setIsLoading(false));

    if ('data' in userData) {
      const { user, token } = userData.data;
      dispatch(setCredentials({ user, token }));
      navigate('/');
    }

    if ('error' in userData) {
      setError((userData.error as ApiError).data.message);
    }
  };

  return (
    <>
      <h1 className={styles.heading}>Sign Up</h1>

      <form className={styles.register_form} onSubmit={handleRegister} noValidate>
        <UnderlineInput
          name="email"
          placeholder="Email address"
          type="email"
          value={registerData.email}
          required
          errorMsg="Invalid email address"
          onChange={handleInputChange}
        />
        <UnderlineInput name="password" placeholder="Password" type="password" value={registerData.password} required onChange={handleInputChange} />
        <UnderlineInput
          name="passwordConfirm"
          placeholder="Repeat password"
          type="password"
          value={registerData.passwordConfirm}
          required
          onChange={handleInputChange}
        />
        <Button>Create an account</Button>
      </form>

      <p className={styles.error}>{error}</p>

      <Text className={styles.text}>
        Already have an account? <Link to="/login">Login</Link>
      </Text>
    </>
  );
};

export default Register;
