import styles from './SelectorInput.module.scss';

import { InputHTMLAttributes } from 'react';
import classNames from 'classnames/bind';

interface SelectorInputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
}

const SelectorInput = ({ name, label, type = 'radio', ...rest }: SelectorInputProps) => {
  const cx = classNames.bind(styles);
  const className = cx(rest.className, 'input-field');

  return (
    <label className={className}>
      <input {...rest} type={type} name={name} />
      {label}
    </label>
  );
};

export default SelectorInput;
