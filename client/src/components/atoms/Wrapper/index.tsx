import styles from './Wrapper.module.scss';

import { HTMLAttributes, ReactNode } from 'react';
import classNames from 'classnames/bind';

interface WrapperProps extends HTMLAttributes<HTMLDivElement> {
  children: ReactNode;
}

const Wrapper = ({ children, ...props }: WrapperProps) => {
  const cx = classNames.bind(styles);

  const className = cx(props.className, 'wrapper');

  return (
    <div {...props} className={className}>
      {children}
    </div>
  );
};

export default Wrapper;
