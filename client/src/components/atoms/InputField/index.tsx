import styles from './InputField.module.scss';

import { InputHTMLAttributes, useState } from 'react';
import classNames from 'classnames/bind';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
  hideErrorNearInput?: boolean;
  hideLabel?: boolean;
  errorMsg?: string;
  icon?: string;
}

const InputField = ({ name, label, icon, className, hideErrorNearInput, hideLabel, errorMsg, required, value, ...props }: InputProps) => {
  const cx = classNames.bind(styles);
  const inputFiledClassName = cx(className, 'input_field');

  const [focused, setFocused] = useState(false);

  return (
    <div className={inputFiledClassName}>
      <input
        autoComplete="off"
        {...props}
        id={name}
        name={name}
        value={value}
        required={required}
        data-focused={focused}
        onBlur={() => setFocused(true)}
      />

      {icon && <img src={icon} alt="icon" />}

      <div>
        {!hideLabel && <label htmlFor={name}>{label}</label>}
        {!hideErrorNearInput && <p>{required && !value ? "can't be empty" : errorMsg}</p>}
      </div>
    </div>
  );
};

export default InputField;
