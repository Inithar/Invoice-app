import styles from './Text.module.scss';

import { HTMLAttributes } from 'react';
import classNames from 'classnames/bind';

interface TextProps extends HTMLAttributes<HTMLParagraphElement> {
  variant?: 'primary' | 'secondary';
}

const Text = ({ children, variant = 'primary', ...props }: TextProps) => {
  const cx = classNames.bind(styles);

  const className = cx(props.className, {
    primary: variant === 'primary',
    secondary: variant === 'secondary'
  });

  return (
    <p {...props} className={className}>
      {children}
    </p>
  );
};

export default Text;
