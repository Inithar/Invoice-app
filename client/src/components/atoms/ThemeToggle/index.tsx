import styles from './ThemeToggle.module.scss';

import { RootState } from '../../../redux/store';
import { setTheme } from '../../../redux/slices/themeSlice';

import { useAppDispatch } from '../../../hooks/useAppDispatch';
import { useAppSelector } from '../../../hooks/useAppSelector';

const ThemeToggle = () => {
  const dispatch = useAppDispatch();
  const currentTheme = useAppSelector((state: RootState) => state.theme);
  const toggledTheme = currentTheme === 'light' ? 'dark' : 'light';

  const toggleTheme = () => {
    dispatch(setTheme(toggledTheme));
  };

  return <img src={`/assets/${toggledTheme}-mode-icon.svg`} alt="theme icon" className={styles.icon} onClick={toggleTheme} />;
};

export default ThemeToggle;
