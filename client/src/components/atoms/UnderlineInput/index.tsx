import styles from './UnderlineInput.module.scss';

import classNames from 'classnames/bind';

import { InputHTMLAttributes, useState } from 'react';

interface UnderlineInputProps extends InputHTMLAttributes<HTMLInputElement> {
  errorMsg?: string;
}

const UnderlineInput = ({ errorMsg, value, required, ...props }: UnderlineInputProps) => {
  const cx = classNames.bind(styles);
  const className = cx(props.className, 'container');

  const [focused, setFocused] = useState(false);

  return (
    <div className={className}>
      <input autoComplete="off" {...props} value={value} required={required} data-focused={focused} onBlur={() => setFocused(true)} />
      <span>{required && !value ? "can't be empty" : errorMsg}</span>
    </div>
  );
};

export default UnderlineInput;
