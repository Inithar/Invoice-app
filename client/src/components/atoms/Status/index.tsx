import styles from './Status.module.scss';

import { HTMLAttributes } from 'react';
import classNames from 'classnames/bind';

import { InvoiceStatus } from '../../../types/invoiceApi';

interface StatusProps extends HTMLAttributes<HTMLDivElement> {
  status: InvoiceStatus;
}

const Status = ({ status, ...props }: StatusProps) => {
  const cx = classNames.bind(styles);
  const className = cx(props.className, 'wrapper', {
    paid: status === 'paid',
    pending: status === 'pending',
    draft: status === 'draft'
  });

  return (
    <div {...props} className={className}>
      <div />
      <p>{status}</p>
    </div>
  );
};

export default Status;
