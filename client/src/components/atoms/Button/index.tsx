import styles from './Button.module.scss';

import { ButtonHTMLAttributes, ReactNode } from 'react';
import classNames from 'classnames/bind';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  variant?: 'primary' | 'secondary' | 'light' | 'dark' | 'danger';
  icon?: boolean;
}

const Button = ({ children, variant = 'primary', icon = false, ...props }: ButtonProps) => {
  const cx = classNames.bind(styles);

  const className = cx(props.className, 'btn', {
    primary: variant === 'primary',
    secondary: variant === 'secondary',
    light: variant === 'light',
    dark: variant === 'dark',
    danger: variant === 'danger',
    icon: icon
  });

  return (
    <button {...props} className={className}>
      {icon && <img src={`/assets/plus-with-background.svg `} alt="plus icon" />}
      {children}
    </button>
  );
};

export default Button;
