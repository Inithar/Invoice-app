import { Navigate, Outlet, useLocation } from 'react-router-dom';

import { useAppSelector } from '../../../hooks/useAppSelector';
import Loader from '../../atoms/Loader';

const RequireAuth = () => {
  const { token, isLoading } = useAppSelector((state) => state.auth);
  const location = useLocation();

  if (isLoading) {
    return <Loader />;
  }

  return token ? <Outlet /> : <Navigate to="/login" state={{ from: location }} replace />;
};

export default RequireAuth;
