import styles from './SelectMenu.module.scss';

import { useEffect, useState } from 'react';
import Text from '../../atoms/Text';

type Value = string | number;

type Option = {
  label: string;
  value: Value;
};

interface SelectMenuProps {
  onChange: (value: Value) => void;
  placeholder?: string;
  selectedValue?: Value | null;
  options: Option[];
}

const SelectMenu = ({ placeholder, options, onChange, selectedValue }: SelectMenuProps) => {
  const [isActive, setIsActive] = useState(false);
  const [selected, setSelected] = useState<Option>({ label: '', value: '' });

  useEffect(() => {
    const selectedOption = options.find((option) => option.value === selectedValue);
    setSelected(selectedOption ? selectedOption : { label: '', value: '' });
  }, []);

  const handleChange = (label: string, value: string | number) => {
    setIsActive(false);
    setSelected({ ...selected, label, value });
    onChange(value);
  };

  return (
    <div className={styles.container}>
      <div className={styles.header} onClick={() => setIsActive(!isActive)}>
        <Text>{selected.label ? selected.label : placeholder}</Text>
        <img src={`/assets/arrow-${isActive ? 'up' : 'down'}.svg`} alt="arrow icon" />
      </div>

      <div className={isActive ? styles.content : styles.hidden}>
        {options.map(({ label, value }) => (
          <div className={styles.option} onClick={() => handleChange(label, value)} key={crypto.randomUUID()}>
            <Text>{label}</Text>
          </div>
        ))}
      </div>
    </div>
  );
};

export default SelectMenu;
