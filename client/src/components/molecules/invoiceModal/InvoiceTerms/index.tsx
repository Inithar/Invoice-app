import styles from './InvoiceTerms.module.scss';

import { useState } from 'react';

import { useAppDispatch } from '../../../../hooks/useAppDispatch';
import { useAppSelector } from '../../../../hooks/useAppSelector';
import { setInvoiceDetails } from '../../../../redux/slices/invoiceSlice';
import formatDate from '../../../../utils/formatDate';

import Calendar from '../../Calendar';
import SelectMenu from '../../SelectMenu';

const paymentTerms = [
  {
    label: 'Net 1 Day',
    value: 1,
  },
  {
    label: 'Net 7 Day',
    value: 7,
  },
  {
    label: 'Net 14 Day',
    value: 14,
  },
  {
    label: 'Net 30 Day',
    value: 30,
  },
];

const InvoiceTerms = () => {
  const dispatch = useAppDispatch();
  const { invoiceDate, daysToPayment } = useAppSelector((state) => state.invoice);
  const [isCalendarOpen, setIsCalendarOpen] = useState(false);

  const handleInvoiceDateChange = (selectedDate: Date) => {
    setIsCalendarOpen(false);
    dispatch(setInvoiceDetails({ key: 'invoiceDate', value: selectedDate.toString() }));
  };

  return (
    <div className={styles.invoice_terms}>
      <div>
        <p className={styles.label}>Invoice Date</p>
        <div className={styles.invoice_date_input} onClick={() => setIsCalendarOpen(!isCalendarOpen)}>
          <p>{invoiceDate ? formatDate(new Date(invoiceDate)) : 'Select date'}</p>
          <img src="/assets/calendar.svg" alt="calendar icon" />
        </div>
      </div>

      {isCalendarOpen && <Calendar className={styles.invoice_date_calendar} onChange={(selectedDate) => handleInvoiceDateChange(selectedDate)} />}

      <div>
        <p className={styles.label}>Payment Terms</p>
        <SelectMenu
          onChange={(value) => dispatch(setInvoiceDetails({ key: 'daysToPayment', value }))}
          options={paymentTerms}
          placeholder="Select payment terms"
          selectedValue={daysToPayment}
        />
      </div>
    </div>
  );
};

export default InvoiceTerms;
