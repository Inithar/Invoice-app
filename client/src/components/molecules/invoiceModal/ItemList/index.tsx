import styles from './ItemList.module.scss';

import useWindowWidth from '../../../../hooks/useWindowWidth';
import useInvoiceFrom from '../../../../hooks/useInvoiceFrom';
import { Item } from '../../../../types/invoiceApi';

import Text from '../../../atoms/Text';
import ItemBox from '../../ItemBox';
import Button from '../../../atoms/Button';

interface ItemListProps {
  items: Item[];
}

const ItemList = ({ items }: ItemListProps) => {
  const isMobile = useWindowWidth() < 576;
  const { handleItemChange, handleDeleteItem, handleNewItem } = useInvoiceFrom();

  return (
    <div>
      <p className={styles.item_list_heading}>Item List</p>

      <div>
        {!isMobile && (
          <div className={styles.head}>
            <Text variant="secondary">Item Name</Text>
            <Text variant="secondary">Qty.</Text>
            <Text variant="secondary">Price</Text>
            <Text variant="secondary">Total</Text>
          </div>
        )}

        {items.map((item) => (
          <ItemBox {...item} onChange={(e) => handleItemChange(e, item._id)} key={item._id} onDelete={handleDeleteItem} />
        ))}

        <Button variant="secondary" type="button" className={styles.add_item_btn} onClick={handleNewItem}>
          + Add New Item
        </Button>
      </div>
    </div>
  );
};

export default ItemList;
