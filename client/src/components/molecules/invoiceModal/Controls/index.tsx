import styles from './Controls.module.scss';

import { Dispatch, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import addDaysToDate from '../../../../utils/addDaysToDate';
import useInvoiceFrom from '../../../../hooks/useInvoiceFrom';
import { useAppSelector } from '../../../../hooks/useAppSelector';
import { useCreateInvoiceMutation, useEditInvoiceMutation } from '../../../../redux/api/invoicesApi';
import { RootState } from '../../../../redux/store';
import { CreateInvoiceArgs, NewInvoice } from '../../../../types/invoiceApi';

import Button from '../../../atoms/Button';

interface ControlsProps {
  type: 'new' | 'edit';
  setErrors: Dispatch<React.SetStateAction<string[]>>;
  closeModal: () => void;
}

const Controls = ({ type, setErrors, closeModal }: ControlsProps) => {
  const isNew = type === 'new';
  const navigate = useNavigate();
  const { id } = useParams();
  const { clearFrom, checkRequiredInputs } = useInvoiceFrom();

  const invoice = useAppSelector((state: RootState) => state.invoice);
  const [createInvoice, createInvoiceDetails] = useCreateInvoiceMutation();
  const [editInvoice, editInvoiceDetails] = useEditInvoiceMutation();

  useEffect(() => {
    if (createInvoiceDetails.isError || editInvoiceDetails.isError) {
      navigate('/error');
    }

    if (createInvoiceDetails.isSuccess || editInvoiceDetails.isSuccess) {
      closeModalAndClearForm();
    }
  }, [createInvoiceDetails, editInvoiceDetails]);

  const closeModalAndClearForm = () => {
    closeModal();
    clearFrom();
    setErrors([]);
  };

  const deleteItemsFakeId = () => invoice.items.map(({ name, quantity, price }) => ({ name, quantity, price }));

  const handleNewInvoice = (asDraft: boolean) => {
    const itemsWithoutFakeId = deleteItemsFakeId();
    const { daysToPayment, ...rest } = invoice;

    let body: CreateInvoiceArgs = { invoice: { ...rest, paymentDue: '', items: itemsWithoutFakeId }, asDraft };

    if (daysToPayment && invoice.invoiceDate) {
      const invoiceDate = new Date(invoice.invoiceDate);
      const paymentDue = addDaysToDate(invoiceDate, daysToPayment).toString();
      body = { invoice: { ...rest, paymentDue, items: itemsWithoutFakeId }, asDraft };
    }

    if (asDraft) {
      body.invoice.status = 'draft';
      createInvoice(body);
    } else {
      const errors = checkRequiredInputs();
      errors.length ? setErrors(errors) : createInvoice(body);
    }
  };

  const handleEditInvoice = () => {
    const errors = checkRequiredInputs();

    if (errors.length) {
      setErrors(errors);
    } else {
      const itemsWithoutFakeId = deleteItemsFakeId();
      const { daysToPayment, ...rest } = invoice;

      const invoiceDate = new Date(invoice.invoiceDate);
      const paymentDue = addDaysToDate(invoiceDate, daysToPayment!).toString();
      const newInvoice: NewInvoice = { ...rest, paymentDue, items: itemsWithoutFakeId, status: 'pending' };

      id ? editInvoice({ invoice: newInvoice, id }) : navigate('/error');
    }
  };

  return (
    <div className={`${styles.controls} ${isNew ? styles.controls_new : styles.controls_edit}`}>
      {isNew ? (
        <>
          <Button type="button" variant="secondary" onClick={closeModalAndClearForm}>
            Discard
          </Button>
          <Button type="button" variant="dark" onClick={() => handleNewInvoice(true)}>
            Save as Draft
          </Button>
          <Button type="button" onClick={() => handleNewInvoice(false)}>
            Save & Send
          </Button>
        </>
      ) : (
        <>
          <Button type="button" variant="secondary" onClick={closeModalAndClearForm}>
            Cancel
          </Button>
          <Button type="button" onClick={handleEditInvoice}>
            Save Changes
          </Button>
        </>
      )}
    </div>
  );
};

export default Controls;
