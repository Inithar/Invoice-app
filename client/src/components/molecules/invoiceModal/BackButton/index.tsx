import styles from './BackButton.module.scss';

import { ButtonHTMLAttributes } from 'react';

const BackButton = ({ ...props }: ButtonHTMLAttributes<HTMLButtonElement>) => (
  <button {...props} className={styles.back_button}>
    <img src="/assets/arrow-left.svg" alt="left arrow icon" />
    Go back
  </button>
);

export default BackButton;
