import styles from './MultiSelectMenu.module.scss';

import { useState } from 'react';
import { InvoiceStatus } from '../../../types/invoiceApi';

import SelectorInput from '../../atoms/SelectorInput';
import Text from '../../atoms/Text';

interface MultiSelectMenuProps {
  onChange: (option: InvoiceStatus, isChecked: boolean) => void;
  options: Record<InvoiceStatus, boolean>;
  placeholder?: string;
}

const MultiSelectMenu = ({ options, placeholder, onChange }: MultiSelectMenuProps) => {
  const [isActive, setIsActive] = useState(false);

  return (
    <div className={styles.container}>
      <div className={styles.header} onClick={() => setIsActive(!isActive)}>
        <Text>{placeholder}</Text>
        <img src={`/assets/arrow-${isActive ? 'up' : 'down'}.svg`} alt="arrow icon" />
      </div>

      <div className={isActive ? styles.content : styles.hidden}>
        {(Object.keys(options) as InvoiceStatus[]).map(option => (
          <SelectorInput
            label={option}
            type="checkbox"
            name="select-option"
            key={crypto.randomUUID()}
            checked={options[option]}
            onChange={e => {
              onChange(option, e.target.checked);
            }}
          />
        ))}
      </div>
    </div>
  );
};

export default MultiSelectMenu;
