import styles from './Calendar.module.scss';

import { useState } from 'react';
import classNames from 'classnames/bind';

import Text from '../../atoms/Text';

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

interface CalendarProps {
  onChange: (date: Date) => void;
  className?: string;
}

const Calendar = ({ onChange, className }: CalendarProps) => {
  const cx = classNames.bind(styles);
  const calendarClassName = cx(className, 'calendar');

  const currentDate = new Date();
  const todayDay = currentDate.getDate();
  const todayMonth = currentDate.getMonth() + 1;

  const [currentYear, setCurrentYear] = useState(currentDate.getFullYear());
  const [currentMonth, setCurrentMonth] = useState(todayMonth);

  const currentMonthIndex = currentMonth - 1;

  const lastDayOfMonth = new Date(currentYear, currentMonth, 0).getDate();
  const lastDayOfMonthLastMonth = new Date(currentYear, currentMonthIndex, 0).getDate();
  const numberOfDaysFromNextMonth = 6 - new Date(currentYear, currentMonthIndex, lastDayOfMonth).getDay();
  const numberOfDaysFromPrevMonth = new Date(currentYear, currentMonthIndex, 1).getDay();

  const daysFromCurrenMonth = Array.from({ length: lastDayOfMonth }, (_, i) => i + 1);
  const daysFromLastMonth = Array.from({ length: numberOfDaysFromPrevMonth }, (_, i) => lastDayOfMonthLastMonth - i).reverse();
  const daysFromNextMonth = Array.from({ length: numberOfDaysFromNextMonth }, (_, i) => i + 1);

  const handleNextMonth = () => {
    if (currentMonth === 12) {
      setCurrentMonth(1);
      setCurrentYear(currentYear + 1);
    } else {
      setCurrentMonth(currentMonth + 1);
    }
  };

  const handlePrevMonth = () => {
    if (currentMonth === 1) {
      setCurrentMonth(12);
      setCurrentYear(currentYear - 1);
    } else {
      setCurrentMonth(currentMonth - 1);
    }
  };

  const handleDateChange = (day: number) => {
    const selectedDate = new Date(currentYear, currentMonth, day);
    onChange(selectedDate);
  };

  return (
    <div className={calendarClassName}>
      <header className={styles.calendar_header}>
        <img src="/assets/arrow-left.svg" alt="left arrow icon" className={styles.icon} onClick={handlePrevMonth} />
        <Text className={styles.current_date}>
          {months[currentMonthIndex]} {currentYear}
        </Text>
        <img src="/assets/arrow-right.svg" alt="right arrow icon" className={styles.icon} onClick={handleNextMonth} />
      </header>
      <div className={styles.calendar_content}>
        <ul className={styles.days}>
          {daysFromLastMonth.map(dayNumber => (
            <li className={styles.inactive_day} key={crypto.randomUUID()}>
              {dayNumber}
            </li>
          ))}

          {daysFromCurrenMonth.map(dayNumber => (
            <li className={dayNumber === todayDay && currentMonth === todayMonth ? styles.active_day : styles.day} key={crypto.randomUUID()} onClick={() => handleDateChange(dayNumber)}>
              {dayNumber}
            </li>
          ))}

          {daysFromNextMonth.map(dayNumber => (
            <li className={styles.inactive_day} key={crypto.randomUUID()}>
              {dayNumber}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Calendar;
