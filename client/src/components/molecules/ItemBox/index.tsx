import styles from './ItemBox.module.scss';

import { ChangeEvent } from 'react';
import { FaTrash } from 'react-icons/fa';

import useWindowWidth from '../../../hooks/useWindowWidth';
import { Item } from '../../../types/invoiceApi';

import InputField from '../../atoms/InputField';

interface ItemBoxProps extends Item {
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  onDelete: (id: string) => void;
}

const ItemBox = ({ _id, name, quantity, price, onChange, onDelete }: ItemBoxProps) => {
  const isMobile = useWindowWidth() < 576;

  const handleZeroValue = (value: number) => {
    return value === 0 ? '' : value;
  };

  return (
    <div className={styles.items}>
      <div className={styles.item}>
        <InputField
          name="name"
          label="Item Name"
          hideErrorNearInput
          hideLabel={!isMobile}
          className={styles.item_name}
          value={name}
          onChange={onChange}
        />
        <InputField
          name="quantity"
          label="Qty."
          type="number"
          hideErrorNearInput
          hideLabel={!isMobile}
          value={handleZeroValue(quantity)}
          onChange={onChange}
        />
        <InputField
          name="price"
          label="Price"
          type="number"
          hideErrorNearInput
          hideLabel={!isMobile}
          value={handleZeroValue(price)}
          onChange={onChange}
        />
        <InputField name="total" label="Total" disabled hideLabel={!isMobile} value={handleZeroValue(quantity * price)} className={styles.total} />
        <FaTrash className={styles.delete_icon} onClick={() => onDelete(_id)} />
      </div>
    </div>
  );
};

export default ItemBox;
