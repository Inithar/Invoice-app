import styles from './InvoiceBox.module.scss';

import { InvoiceStatus, Item } from '../../../types/invoiceApi';
import countTotalAmount from '../../../utils/countTotalAmount';
import formatDate from '../../../utils/formatDate';

import Status from '../../atoms/Status';
import Text from '../../atoms/Text';

interface InvoiceBoxProps {
  id: string;
  clientName: string;
  status: InvoiceStatus;
  paymentDue: string;
  items: Item[];
}

const InvoiceBox = ({ id, clientName, status, items, paymentDue }: InvoiceBoxProps) => (
  <div className={styles.wrapper}>
    <p className={styles.id}>
      <span>#</span>
      {id}
    </p>
    <Text variant="secondary" className={styles.date}>
      {paymentDue ? `Due ${formatDate(new Date(paymentDue))}` : ''}
    </Text>
    <Text variant="secondary" className={styles.name}>
      {clientName}
    </Text>
    <Text className={styles.amount}>£ {countTotalAmount(items)}</Text>
    <Status className={styles.status} status={status} />
  </div>
);

export default InvoiceBox;
