import { Item } from '../types/invoiceApi';

const countTotalAmount = (items: Item[]) => {
  return items.reduce((accumulator, { quantity, price }) => accumulator + quantity * price, 0);
};

export default countTotalAmount;
