import api from '.';
import { UserWithToken } from '../../types/authApi';

const authApi = api.injectEndpoints({
  endpoints: (builder) => ({
    login: builder.mutation<UserWithToken, { email: string; password: string }>({
      query: (body) => ({
        url: '/auth/login',
        method: 'POST',
        body,
      }),
    }),

    register: builder.mutation<UserWithToken, { email: string; password: string; passwordConfirm: string }>({
      query: (body) => ({
        url: '/auth/register',
        method: 'POST',
        body,
      }),
    }),

    logOut: builder.mutation<{ success: string }, void>({
      query: () => ({
        url: '/auth/logout',
        method: 'POST',
      }),
    }),

    isAuthenticated: builder.query<UserWithToken, void>({
      query: () => '/auth/isAuthenticated',
    }),
  }),
});

export const { useLoginMutation, useRegisterMutation, useLogOutMutation, useLazyIsAuthenticatedQuery } = authApi;
