import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const baseQuery = fetchBaseQuery({ baseUrl: 'https://invoice-app-ggo6.onrender.com/api/v1/', credentials: 'include' });

const api = createApi({
  baseQuery,
  tagTypes: ['Invoice'],
  endpoints: () => ({}),
});

export default api;
