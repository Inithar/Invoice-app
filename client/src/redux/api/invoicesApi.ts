import api from '.';
import { CreateInvoiceArgs, EditInvoiceArgs, Invoice, NewInvoice } from '../../types/invoiceApi';

const invoicesApi = api.injectEndpoints({
  endpoints: (builder) => ({
    getAllInvoices: builder.query<Invoice[], void>({
      providesTags: [{ type: 'Invoice', id: 'LIST' }],
      query: () => '/invoices',
    }),

    getInvoice: builder.query<Invoice, string | undefined>({
      providesTags: ['Invoice'],
      query: (id) => `/invoices/${id}`,
    }),

    createInvoice: builder.mutation<Invoice, CreateInvoiceArgs>({
      invalidatesTags: [{ type: 'Invoice', id: 'LIST' }],
      query: (body) => ({
        url: '/invoices',
        method: 'POST',
        body,
      }),
    }),

    editInvoice: builder.mutation<Invoice, EditInvoiceArgs>({
      invalidatesTags: ['Invoice'],
      query: ({ invoice, id }) => ({
        url: `/invoices/${id}`,
        method: 'PATCH',
        body: invoice,
      }),
    }),

    deleteInvoice: builder.mutation<{ success: boolean }, string>({
      invalidatesTags: [{ type: 'Invoice', id: 'LIST' }],
      query: (id) => ({
        url: `/invoices/${id}`,
        method: 'DELETE',
      }),
    }),
  }),
});

export const { useGetAllInvoicesQuery, useGetInvoiceQuery, useCreateInvoiceMutation, useEditInvoiceMutation, useDeleteInvoiceMutation } = invoicesApi;
