import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { User } from '../../types/authApi';

interface InitialState {
  token: string | null;
  user: User | null;
  isLoading: boolean;
}

const initialState: InitialState = {
  token: null,
  user: null,
  isLoading: true,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setCredentials: (state, action: PayloadAction<{ user: User; token: string }>) => {
      const { user, token } = action.payload;
      state.user = user;
      state.token = token;
    },

    logOut: (state) => {
      state.user = null;
      state.token = null;
    },

    setIsLoading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload;
    },
  },
});

export const { setCredentials, logOut, setIsLoading } = authSlice.actions;
export default authSlice.reducer;
