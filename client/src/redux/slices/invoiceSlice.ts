import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Address, InvoiceStatus, Item } from '../../types/invoiceApi';

type KeyValueObj = { key: string; value: string | number };
type KeyValueIdObj = { key: string; value: string; id: string };

interface InitialState {
  status: InvoiceStatus;
  senderAddress: Address;
  clientAddress: Address;
  clientName: string;
  clientEmail: string;
  invoiceDate: string;
  description: string;
  items: Item[];
  daysToPayment: number | null;
}

const initialState: InitialState = {
  status: 'pending',
  senderAddress: { street: '', city: '', postCode: '', country: '' },
  clientAddress: { street: '', city: '', postCode: '', country: '' },
  clientName: '',
  clientEmail: '',
  invoiceDate: '',
  daysToPayment: null,
  description: '',
  items: [],
};

export const invoiceSlice = createSlice({
  name: 'invoice',
  initialState,
  reducers: {
    setSenderAddressProperty: (state, action: PayloadAction<KeyValueObj>) => ({
      ...state,
      senderAddress: { ...state.senderAddress, [action.payload.key]: action.payload.value },
    }),

    setClientAddressProperty: (state, action: PayloadAction<KeyValueObj>) => ({
      ...state,
      clientAddress: { ...state.clientAddress, [action.payload.key]: action.payload.value },
    }),

    setInvoiceDetails: (state, action: PayloadAction<KeyValueObj>) => ({
      ...state,
      [action.payload.key]: action.payload.value,
    }),

    addItem: (state) => ({
      ...state,
      items: [...state.items, { _id: `${state.items.length}`, name: '', quantity: 0, price: 0 }],
    }),

    deleteItem: (state, action: PayloadAction<string>) => ({
      ...state,
      items: state.items.filter((item) => item._id !== action.payload),
    }),

    setItem: (state, action: PayloadAction<KeyValueIdObj>) => {
      const newItems = state.items.map((item) => {
        return item._id === action.payload.id ? { ...item, [action.payload.key]: action.payload.value } : item;
      });

      return {
        ...state,
        items: newItems,
      };
    },

    setInitialState: () => initialState,
    setInvoice: (state, action: PayloadAction<InitialState>) => action.payload,
  },
});

export const { setSenderAddressProperty, setClientAddressProperty, setInvoiceDetails, setItem, addItem, deleteItem, setInitialState, setInvoice } =
  invoiceSlice.actions;
export default invoiceSlice.reducer;
