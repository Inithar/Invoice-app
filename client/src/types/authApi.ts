export interface User {
  _id: string;
  email: string;
}

export interface UserWithToken {
  user: User;
  token: string;
}
