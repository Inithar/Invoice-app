export type InvoiceStatus = 'paid' | 'pending' | 'draft';

export type Address = {
  street: string;
  city: string;
  postCode: string;
  country: string;
};

export type ItemWithoutFakeId = {
  name: string;
  quantity: number;
  price: number;
};

export interface Item extends ItemWithoutFakeId {
  _id: string;
}

export interface InvoiceDetails {
  status: InvoiceStatus;
  senderAddress: Address;
  clientAddress: Address;
  clientName: string;
  clientEmail: string;
  invoiceDate: string;
  paymentDue: string;
  description: string;
}

export interface Invoice extends InvoiceDetails {
  _id: string;
  shortId: string;
  items: Item[];
}

export interface NewInvoice extends InvoiceDetails {
  items: ItemWithoutFakeId[];
}

export interface CreateInvoiceArgs {
  invoice: NewInvoice;
  asDraft: boolean;
}

export interface EditInvoiceArgs {
  invoice: NewInvoice;
  id: string;
}
