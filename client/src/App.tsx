import './scss/index.scss';

import { useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';

import { useAppSelector } from './hooks/useAppSelector';
import { RootState } from './redux/store';

import Layout from './components/templates/Layout';
import Home from './components/pages/Home';
import Details from './components/pages/Details';
import Error from './components/pages/Error';
import Auth from './components/templates/Auth';
import Login from './components/pages/Login';
import Register from './components/pages/Register';
import RequireAuth from './components/molecules/RequireAuth';
import useAuth from './hooks/useAuth';

const App = () => {
  const theme = useAppSelector((state: RootState) => state.theme);
  const { isAuthenticated } = useAuth();

  useEffect(() => {
    isAuthenticated();
  }, []);

  useEffect(() => {
    document.querySelector('body')?.setAttribute('data-theme', theme);
    localStorage.setItem('selectedTheme', theme);
  }, [theme]);

  return (
    <Routes>
      <Route element={<Auth />}>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Route>

      <Route element={<RequireAuth />}>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="invoices/:id" element={<Details />} />
        </Route>
      </Route>

      <Route path="/error" element={<Error type="error" />} />
      <Route path="*" element={<Error type="notFound" />} />
    </Routes>
  );
};

export default App;
